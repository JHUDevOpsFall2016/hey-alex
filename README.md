# hey
hey is a tiny program that sends some load to a web application.

## Dependencies
Targets installation is on Centos 7.

* Requires go 1.7 or greater.
* Ansible

## Build
hey can be built as a standalone binary:
```
cd hey
go build
go test
./hey
```
... or hey can be installed in a vagrant VM:
```
vagrant up
vagrant ssh
/vagrant/hey/hey
```
## Usage

hey runs provided number of requests in the provided concurrency level and prints stats.

It also supports HTTP2 endpoints.

```
Usage: hey [options...] <url>

Options:
  -n  Number of requests to run.
  -c  Number of requests to run concurrently. Total number of requests cannot
      be smaller than the concurrency level.
  -q  Rate limit, in seconds (QPS).
  -o  Output type. If none provided, a summary is printed.
      "csv" is the only supported alternative. Dumps the response
      metrics in comma-separated values format.

  -m  HTTP method, one of GET, POST, PUT, DELETE, HEAD, OPTIONS.
  -H  Custom HTTP header. You can specify as many as needed by repeating the flag.
      for example, -H "Accept: text/html" -H "Content-Type: application/xml".
  -t  Timeout in seconds. Default is 20, use 0 to disable.
  -A  HTTP Accept header.
  -d  HTTP request body.
  -T  Content-type, defaults to "text/html".
  -a  Basic authentication, username:password.
  -x  HTTP Proxy address as host:port.

  -h2  Make HTTP/2 requests.

  -disable-compression  Disable compression.
  -disable-keepalive    Disable keep-alive, prevents re-use of TCP
                        connections between different HTTP requests.
  -cpus                 Number of used cpu cores.
  -host                 HTTP Host header.
  -more                 Provides information on DNS lookup, dialup, request and response timings.

  ```
